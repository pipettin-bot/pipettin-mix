# Pipetting sequence generator

Generates information to prepare solutions with fixed and variable components (i.e. different conditions), using hierarchical clustering.

The PCR Mix protocol planner clusters tubes by similarity into a dendrogram. The resulting tree is cut, and intermediate solutions are prepared, reducing the amount of required pipetting steps, and a consequently faster protocol. 

For examples see: [mix/usage.ipynb](mix/usage.ipynb)

A simple test script can be run from the modules top directory with `python -m mix.test`.

Status: pre-release, under development.

There is an R version of this here: https://gitlab.com/naikymen/cuentitas-helper

![dendro_planner.png](./images/dendro_planner.png)

This software is distributed under the terms of the GNU Affero GPLv3 license. See [LICENSE.txt](./LICENSE.txt).
