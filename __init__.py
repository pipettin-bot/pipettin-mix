# The __init__.py files are required to make Python treat directories containing the file as packages. 
# This prevents directories with a common name, such as string, from unintentionally hiding valid modules
# that occur later on the module search path. In the simplest case, __init__.py can just be an empty file, 
# but it can also execute initialization code for the package or set the __all__ variable, described later.

# When packages are structured into subpackages (as with the sound package in the example), you can use 
# absolute imports to refer to submodules of siblings packages.

# Note that relative imports are based on the name of the current module. Since the name of the main module
# is always "__main__", modules intended for use as the main module of a Python application must always use 
# absolute imports.

# https://docs.python.org/3/tutorial/modules.html
# https://dev.to/codemouse92/dead-simple-python-project-structure-and-imports-38c6
