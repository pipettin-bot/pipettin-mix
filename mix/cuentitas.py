# Copyright (C) 2023 Nicolás A. Méndez
#
# This file is part of "mix".
#
# "mix" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "mix" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "mix". If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=locally-disabled, line-too-long, logging-fstring-interpolation, logging-not-lazy, fixme, multiple-imports, wrong-import-position, bad-indentation

from dataclasses import dataclass
from pprint import pprint
import pandas
from copy import deepcopy
from math import isclose
from collections import OrderedDict as odict

from .component_groups import *
from .cluster import *

def comps(**kwargs):
    """Generate dict from keyword args.
    Same as 'kwtodict' and 'groups'."""
    return odict((k, v) for k,v in kwargs.items())

def kwtodict(**kwargs):
    """Generate dict from keyword args.
    Same as 'comps' and 'groups'."""
    return odict((k, v) for k,v in kwargs.items())

def flatten(nested_list):
    """Merge a list of lists into a single unnested list."""
    return [item for sublist in nested_list for item in sublist]

def make_dict(names, values):
    """Make a dictionary from a list of key names and a list of values."""
    return odict((k, v) for k,v in zip(names,values))

def all_same(values):
    """Check if all values in a list are equal."""
    return len(set(values)) == 1

def get_comp_names(component_groups):
  """Iterate over the component groups, gathering all component names, and return a deduplicated set."""
  comps_list = flatten([g.keys() for g in component_groups.values()])
  unique_comps_list = list(set(comps_list))
  return unique_comps_list

def get_variable_components(component_groups: dict,
                            comp_groups:pandas.DataFrame=None,
                            variable_components: dict=None,
                            replace_missing="0",
                            keep_constants=True):
  """Generate a dataframe of variable components and reaction groups from the 'component_groups' dictionary.

  This is the 'inverse' of the 'groups_to_comps' function.

  Note that 'expand_groups' only generates 'variable_components' which effectively
  generate combinations (during expand_grid) Because of this a _constant component_
  (i.e. single-valued) will not be kept, and will not appear in the _variable components_ dict.
  Set `keep_constants=True` to disable this behaviour.
  """
  # Lazy variable rename (sorry).
  comp_groups_df = comp_groups

  if comp_groups_df is None or variable_components is None:
    comp_groups_df, value_columns, variable_columns, variable_components = expand_groups(
      component_groups=component_groups,
      replace_missing=replace_missing,
      keep_constants=keep_constants)

  # Remove common components from candidates.
  common_comps = [col for col in comp_groups_df if all_same(comp_groups_df[col].tolist())]
  comp_groups_df = comp_groups_df.drop(common_comps, axis=1)

  # Remove 'variable_components' which appear to be constant.
  if not keep_constants:
    _ = [variable_components.pop(k, None) for k in common_comps]

  # Recreate variable_component_names.
  variable_component_names = list(variable_components)

  return variable_components, variable_component_names, comp_groups_df

def groups_to_comps(component_groups_df: pandas.DataFrame,
                    variable_component_names: list,
                    grouping_col="group"):
    """ Generate a 'component_groups' dictionary using expand_groups's component groups dataframe
    and a list of the variable component names.

    This is the 'inverse' of the 'get_variable_components' function.
    """
    # Group the input dataframe by "component group name".
    grouped_groups = component_groups_df.groupby(component_groups_df[grouping_col])
    # Generate the 'component_groups' dictionary.
    # r = {g[0]: {comp: tuple(set(g[1][comp].tolist())) for comp in variable_component_names} for g in grouped_groups}
    r = odict( (g[0], odict( (comp, tuple(set(g[1][comp].tolist()))) for comp in variable_component_names )) for g in grouped_groups )
    return r

def make_component_groups(variable_components):
  """Generate 'component_groups' from 'variable_components'."""
  # component_groups = { f"default": {key: c(vals) for key,vals in variable_components.items()} }
  component_groups = {"default": odict( (key, c(vals)) for key, vals in variable_components.items() ) }
  return component_groups

def setdiff(l1, l2):
  """Get set elements from l1 (first list) not in l2 (second list).

  Args:
      l1 (list, dict): First list of items. Keys are used if a Dict is passed.
      l2 (list, dict): Second list of items. Keys are used if a Dict is passed.

  Returns:
      list: Set elements from l1 (first list) not in l2 (second list).
  """
  l1_only = list(set(list(l1)).difference(set(list(l2))))
  return [x for x in l1 if x in l1_only] # Preserve order

def list_pop(the_list, val):
   """Remove values from 'the_list' equal to a query value."""
   return [value for value in the_list if value != val]


@dataclass  # https://stackoverflow.com/a/71189560/11524079
class Recipe:
  """Object defining a recipe to prepare solutions.

  Example:

  components = Recipe(
    component_groups = component_groups,
    extra_fraction = 0.3,
    volume = 20,
    stock_solutions_x = comps(
      buffer = 10,      # 10 x
      pol = 100,        # 100x (0.2 uL / 20 uL = 0.01)
      dNTPs = 10,       # 10 mM stock
      pFw = 10,         # uM
      pRv = 10          # uM
    ),
    stock_solutions_vol = comps(
      template = 1      # uL
    ),
    target_solution = comps(
      dNTPs = 0.1,      # 0.1 mM final (10 mM is 100x). Recomiendan 50-200 uM, normalmente uso 0.2 final / 50 x.
      pFw = 0.2,        # uM (50 x)
      pRv = 0.2         # uM (50 x)
    ),
    variable_components = comps(
      template = c("A139", "A709", "A710", "A602" ),
      pFw = c(432, 1152),
      pRv = c(433, 282)
    )
  )
  """

  # From "cuentitas4"
  n_reactions: int=None
  "Target amount of reactions to prepare."
  n_extra: int=None
  "Extra reactions to prepare (accounting for loss of volume). This overrides 'extra_fraction'."
  volume: float=None
  "Total reaction volume (NULL only if all components are defined in stock_solutions_vol)."
  stock_solutions_x: dict = None
  "Concentration of components in source solutions (interpreted as 'X' fold, if undefined in target_solution, see examples)."
  stock_solutions_vol: dict = None
  "Fixed volume of components to be used in each reaction (see examples)."
  target_solution: dict = None
  "Final concentration of components defined in stock_solutions_x (optional, see notes in stock_solutions_x, see examples)."
  variable_components: dict = None
  "Named list of components which must be left out of the master mix solution (see examples)."
  units: str="uL"
  "A string used to format volume in messages (default 'uL')."
  extra_fraction: float=0.2
  "Extra fraction of the total volume to prepare, to account for volume loss during transfers. Overriden by 'n_extra'."
  component_groups: dict = None
  "Dictionary defining 'groups' of variable components."
  # Extra variable components
  variable_components: dict = None
  variable_component_names: list = None
  verbose: bool = False

  def __post_init__(self):
    # NOTE: this will run after the dataclasse's init method.
    # https://stackoverflow.com/a/51248309/11524079

    # Specifying "n_extra" will require specifying "n_reactions".
    # Leaving it as "None" (the default) will raise an error here.
    if self.n_extra is not None:
      self.extra_fraction = self.n_extra / self.n_reactions

    # Set defaults / check data
    self.stock_solutions_x = self.check_data(self.stock_solutions_x)
    self.stock_solutions_vol = self.check_data(self.stock_solutions_vol)
    self.target_solution = self.check_data(self.target_solution)
    self.variable_components = self.check_data(self.variable_components)

    # Generate "variable_components" from "component_groups" or viceversa.
    self.fill_components()

    # Make list of all components.
    # NOTE: Some python behaviours things are annoying.
    self.recipe_component_names = list(set(
      list(self.stock_solutions_x.keys()) +
      list(self.stock_solutions_vol.keys()) +
      list(self.target_solution.keys()) +
      list(self.variable_components.keys())
    ))

    # Make a list of the components declared in the component groups dict.
    self.component_names = get_comp_names(self.component_groups)

    # Check that all components have an amount in the recipe.
    check_comps = setdiff(self.component_names, self.recipe_component_names)
    if check_comps:
      raise ValueError(f"Error: the following components are missing in the recipe definition: {check_comps}")

    # Infer the number of reactions if it was not specified explicitly.
    if self.n_reactions is None:
      expanded_groups = expand_groups(component_groups=self.component_groups)[0]
      self.n_reactions = len(expanded_groups.index)

  #### Main methods ####

  def fill_components(self):
    """Generate "variable_components" from "component_groups" or viceversa.
    Validates input arguments dring the classe's 'post_init'.
    """
    if self.component_groups:
      # Set 'variable_components' from the provided 'component_groups'.
      if self.variable_components:
        # Warn override.
        msg = "Warning: Both 'variable_components' and 'component_groups' were set. "
        msg += "The original 'variable_components' will be updated."
        if self.verbose: print(msg)
      else:
        msg = "Message: generating 'variable_components' from 'component_groups'."
        if self.verbose: print(msg)
      self.set_variable_components()
    else:
      # Set 'component_groups' from the provided 'variable_components'.
      if not self.variable_components:
        raise ValueError("Error: neither 'variable_components' and 'component_groups' were set.")
      msg = "Message: generating 'component_groups' from 'variable_components'."
      if self.verbose: print(msg)
      self.set_component_groups()

    # Update variable component names attribute.
    self.update_variable_component_names()

  def set_variable_components(self):
    """Update "variable_components" with "component_groups" and viceversa.
    This function fires when both are specified.
    """

    # Iterate over the component groups.
    if self.verbose:
      pprint(self.component_groups)  # 1
    for component_group in self.component_groups.values():
      # And add variable component to each one (if not found).
      for vc_name, vc_content in deepcopy(self.variable_components).items():
        component_group.setdefault(vc_name, vc_content)
    if self.verbose:
      pprint(self.component_groups)  # 2

    # Get the unique set of variable components from the updated 'component_groups'.
    component_groups = deepcopy(self.component_groups)  # Work on a copy just in case.
    var_comps, self.variable_component_names, _ = get_variable_components(
      component_groups=component_groups,
      keep_constants=False)
    if self.verbose:
      print("set_variable_components result:")
      pprint(var_comps)  # 3
      pprint(self.variable_component_names) # 3

    # TODO: Review. had to add 'deepcopy' here because 'update' below inadvertedtly
    # modified 'self.component_groups' by some referenced innner dictionaries.
    self.variable_components = deepcopy(self.variable_components)

    # Update the 'variable_components' dict from the class' init argument
    # with the ones derived here from the 'component_groups' dict.
    if self.verbose:
      print("set_variable_components: self.variable_components before update.")
      pprint(self.variable_components)  # 4

    self.variable_components.update(var_comps)

    if self.verbose:
      print("set_variable_components: self.variable_components after update.")
      pprint(self.variable_components)  # 5

  def to_dict(self):
    """Generate a dictionary from the classe's input attributes.

    The output dictionary can be expanded to instantiate a new equivalent Recipe.
    """
    inputs = {
      # From "cuentitas4"
      "n_reactions": self.n_reactions,
      "n_extra": self.n_extra,
      "volume": self.volume,
      "stock_solutions_x": self.stock_solutions_x,
      "stock_solutions_vol": self.stock_solutions_vol,
      "target_solution": self.target_solution,
      "units": self.units,
      "extra_fraction": self.extra_fraction,
      # Dictionary defining "groups" of variable component.
      "component_groups": self.component_groups,
      # Extra variable components
      "variable_components": self.variable_components
    }
    inputs_copy = deepcopy(inputs)

    return inputs_copy

  #### Misc utility functions ####

  def update_variable_component_names(self):
    """Update variable component names attribute."""
    self.variable_component_names = list(self.variable_components.keys())

  def set_component_groups(self):
    """Generate 'component_groups' from 'variable_components'."""
    variable_components = deepcopy(self.variable_components)
    self.component_groups = make_component_groups(variable_components)

  @staticmethod
  def check_data(data) -> dict:
    if data is None:
      data = odict()
    return data

class Mix():
  def __init__(self,
               recipe: Recipe,
               component_groups:dict=None,
               print_recipe=True,
               fix_comps_ref: dict= None,
               verbose=False):
    self.verbose = verbose

    # Save the original recipe and a copy to modify later.
    self.original_recipe = deepcopy(recipe)
    self.recipe = deepcopy(recipe)
    # Reference names of component variants that changed in a parent mix.
    self.fix_comps_ref = fix_comps_ref
    # Labels for mix and water
    self.solvent = "water"
    self.mix_label = "mix"

    # Override component groups if requested
    if component_groups is not None:
      if self.recipe.component_groups:
        if self.verbose: print("Warning: the recipe already contains 'component_groups'.")
        # ValueError("The recipe already contains 'component_groups'.")
      self.recipe.component_groups = component_groups

    # Check that component_groups is not none.
    if not self.recipe.component_groups:
      raise ValueError("The recipe's 'component_groups' are not set.")

    # Get the mix information
    self.mix_info: dict = self.make_mix_info()

    # Save the reduced "mix" recipe.
    self.mix_recipe: Recipe = self.make_mix_recipe()   # Uses 'Recipe.serialize'.

    if print_recipe:
      self.recipe_msg()

  def make_mix_info(self):
    """
    Calculate the amounts of each component in "self.recipe" needed to
    prepare a "mix solution" with all "fixed" components in the recipe,
    and calculates the amounts of each "variable" component that must be
    added to that mix (at some point) to complete the preparation.
    """

    # Recompute total number of reactions.
    # n_total = self.recipe.n_reactions + self.recipe.n_extra
    n_total = self.recipe.n_reactions * (1.0 + self.recipe.extra_fraction)

    # Check if we must do cuentitas from stock_solutions_vol total volume,
    # else we will use the provided total volume.
    if not self.recipe.volume:

      # Total reaction volume (per tube).
      self.recipe.volume = sum(self.recipe.stock_solutions_vol.values())

      # Check that all names in "stock_solutions_x" have a "volume" in "stock_solutions_vol".
      # We dont want the inadverted user forgetting to set "volume" in that case.
      if(len(list(self.recipe.stock_solutions_x)) > 0):
        if len(setdiff(self.recipe.stock_solutions_x, self.recipe.stock_solutions_vol)) > 0:
          raise ValueError("Null volume requires all components in stock_solutions_x to have a volume in stock_solutions_vol.")

      # Check no variable components are without a source volume.
      if(len(list(self.recipe.variable_components)) > 0):
        if len(setdiff(self.recipe.variable_components,
                       self.recipe.stock_solutions_vol)) > 0:
          raise ValueError("Not all variable_components names are in stock_solutions_vol. This is required for calculations.")
      else:
        if self.verbose:
          print("Note: variable_components is empty.")

    # Total volume (all tubes summed).
    total_vol = self.recipe.volume * n_total

    # Check if any final concentrations have a stock concentration.
    if len(setdiff(self.recipe.stock_solutions_x, self.recipe.target_solution)) > 0:
      if self.verbose:
        print("Note: all 'stock_solutions_x' components with no entry in 'target_solution' are interpreted in 'fold' concentration units.")

    # Convert stock_solutions_vol to fold concentration (X).
    for n in list(self.recipe.stock_solutions_vol):
      self.recipe.stock_solutions_x[n] = self.recipe.volume / self.recipe.stock_solutions_vol[n]

    # Get (all) component names.
    component_names = list(self.recipe.stock_solutions_x)
    # Get variable component names.
    variable_components_names = list(self.recipe.variable_components)
    # Get (fixed) mix component names.
    mix_components =  setdiff(component_names, variable_components_names)
    # Add water if not already in the list.
    if self.solvent not in mix_components:
        mix_components = [self.solvent] + mix_components

    # Check no variable components are without a source volume.
    if(len(variable_components_names) > 0):
      if len(setdiff(variable_components_names,
                     component_names)) > 0:
        raise ValueError("Not all variable_components names are in stock_solutions_x/vol. This is required for calculations.")
    else:
      if self.verbose: print("Note: variable_components is empty.")

    # Check that no target concentrations are without a source concentration.
    if(len(self.recipe.target_solution) > 0):
      if len(setdiff(self.recipe.target_solution, component_names)) > 0:
        # If not all names in "target_solution" are in "component_names", error-out.
        raise ValueError("Not all target_solution names are in stock_solutions_x/vol. This is required for calculations.")

    # Default target concentration to 1 for unspecified target concentrations
    # (assume the F was given in the stock specification).
    diff_names = setdiff(component_names, self.recipe.target_solution)
    for n in diff_names:
      self.recipe.target_solution[n] = 1.0

    # Calculate component volumes for mix.
    # component_volumes = {n: total_vol * self.recipe.target_solution[n] / self.recipe.stock_solutions_x[n] for n in component_names}
    component_volumes = odict((n, total_vol * self.recipe.target_solution[n] / self.recipe.stock_solutions_x[n]) for n in component_names)

    # Add water to the mix components if necessary.
    water_volume = total_vol - sum(component_volumes.values())
    if isclose(water_volume, 0.0, rel_tol=1e-09, abs_tol=1e-5):
      mix_components = list_pop(mix_components, self.solvent)
    else:
      component_volumes[self.solvent] = water_volume

    # Calculate mix volume to be used.
    variable_components_vol = sum([component_volumes[n] for n in variable_components_names])
    mix_vol = total_vol - variable_components_vol  # total
    mix_vol_per_rxn = mix_vol / n_total # per reaction

    # Make a dictionary with some general output variables.
    general_info = kwtodict(n_reactions = self.recipe.n_reactions,
                            n_extra = self.recipe.n_extra,
                            extra_fraction = self.recipe.extra_fraction,
                            total_vol = total_vol,
                            mix_volume = mix_vol,
                            reaction_volume = self.recipe.volume,
                            mix_vol_per_rxn = mix_vol_per_rxn,
                            mix_x = self.recipe.volume / mix_vol_per_rxn,
                            fix_comps_ref=self.fix_comps_ref)

    # mix_component_volumes = {n: component_volumes[n] for n in mix_components}
    # fixed_component_rxn_volumes = { n: component_volumes[n] / n_total for n in mix_components }
    # variable_component_rxn_volumes = { n: component_volumes[n] / n_total for n in variable_components_names }
    mix_component_volumes = odict( (n, component_volumes[n]) for n in mix_components )
    fixed_component_rxn_volumes = odict( (n, component_volumes[n] / n_total) for n in mix_components )
    variable_component_rxn_volumes = odict( (n, component_volumes[n] / n_total) for n in variable_components_names )

    # Create results list.
    intermix = {"general_info": general_info,
                "mix_component_volumes": mix_component_volumes,
                "fixed_component_rxn_volumes": fixed_component_rxn_volumes,
                "variable_component_rxn_volumes": variable_component_rxn_volumes,
                # Add total volumes
                "total_component_volumes": component_volumes}

    return intermix

  def get_fxdcmp(self, key):
    """Get a reference string from the name of a mix component volume (e.g. in 'self.mix_info["mix_component_volumes"]')."""
    fix_comps_ref = self.mix_info["general_info"]["fix_comps_ref"]
    ref_txt = ""
    if fix_comps_ref:
      ref_name = fix_comps_ref.get(key, None)
      if ref_name:
        ref_txt = f" ({ref_name})"
    return ref_txt

  def recipe_msg(self):
    """Prints out a nice, readable recipe to prepare the mix.

    Args:
        components (_type_): _description_
        mix_component_volumes (_type_): _description_
        mix_vol_per_rxn (_type_): _description_
        variable_component_rxn_volumes (_type_): _description_
    """
    var_comps = self.mix_recipe.variable_components
    mix_vol_per_rxn = self.mix_info["general_info"]["mix_vol_per_rxn"]
    mix_comp_vols = self.mix_info["mix_component_volumes"]
    var_comp_vols = self.mix_info["variable_component_rxn_volumes"]
    units = self.recipe.units

    msg = ""
    msg += "\nMix components:\n"
    msg += "\n".join([f"   {mix_comp_vols[n]} {units} {n}{self.get_fxdcmp(n)}" for n in list(mix_comp_vols)])
    msg += "\n\n"

    msg += f"Use {mix_vol_per_rxn} {units} of the mix for each reaction.\n\n"

    if len(list(self.recipe.variable_components)) > 0:
      msg += "To each individual reaction add [up to]:\n"
      msg += "\n".join([f"   {var_comp_vols[n]} {units} {n} ({', '.join(var_comps[n])})" for n in list(var_comp_vols)])
      msg += "\n"

    print(msg)

  def make_preparation(self, base_mix_name="main", parent_mix_name=None, preparations:list=None, level=0, parent_label=""):
    mix_comp_vols = self.mix_info["mix_component_volumes"]
    fix_comps_ref = self.mix_info["general_info"]["fix_comps_ref"]
    var_comps = self.mix_recipe.variable_components

    if parent_mix_name:
      label = "-".join([parent_label] + [ f"{k}{v}" for k, v in fix_comps_ref.items() ])
      base_mix_name = label
    else:
      label = base_mix_name

    preparation = {"name": base_mix_name,
                   "parent": parent_mix_name,
                   "recipe": mix_comp_vols,
                   "fixed_component_reference": fix_comps_ref,
                   "label": label,
                   "derivatives": []}
    preparation["volume"] = sum(preparation["recipe"].values())

    # List of preparations.
    if preparations is not None:
      # Append the recipe.
      r = {'name': preparation['name'],
           'recipe': preparation['recipe'],
           'level': level}
      preparations.append(r)

    return preparation

  def make_mix_recipe(self):
    """Prepare new Recipe, replacing 'fixed' components with a single 'mix' solution.

    This uses the Recipe's serialize method, in order to re-create a new Recipe object from scratch.

    Returns:
        Mix: Copy of the parent Recipe instance, with mix components replaced by a single "intermix" component.
    """
    # reduced_recipe
    reduced_recipe_dict: dict = self.original_recipe.to_dict()  # Put the classe's objects in a dict.

    if self.mix_info is None:
      self.mix_info = self.make_mix_info()

    fixed_component_names = list(self.mix_info['fixed_component_rxn_volumes'])
    variable_component_names = list(self.mix_info['variable_component_rxn_volumes'])

    # Remove fixed components from the Recipe's solutions.
    for mix_comp_name in fixed_component_names:
      reduced_recipe_dict["target_solution"].pop(mix_comp_name, None)
      reduced_recipe_dict["stock_solutions_vol"].pop(mix_comp_name, None)
      reduced_recipe_dict["stock_solutions_x"].pop(mix_comp_name, None)

    # Get general info
    general_info = self.mix_info["general_info"]

    # Recompute total number of reactions.
    # n_total = general_info["n_reactions"] + general_info["n_extra"]
    n_total = general_info["n_reactions"] * (1.0 + general_info["extra_fraction"])

    # Add a "mix" component to the recipe (name set by "self.mix_label").
    reduced_recipe_dict["volume"] = general_info["total_vol"] / n_total
    reduced_recipe_dict["stock_solutions_x"][self.mix_label] = self.mix_info['general_info']['mix_x']
    reduced_recipe_dict["target_solution"][self.mix_label] = 1.0

    # Move "mix" to the start of the odicts
    reduced_recipe_dict["stock_solutions_x"].move_to_end(self.mix_label, last=False)
    reduced_recipe_dict["target_solution"].move_to_end(self.mix_label, last=False)

    # Remove fixed components from 'component_groups'.
    for component_group in reduced_recipe_dict["component_groups"].values():
      for mix_comp_name in fixed_component_names:
        component_group.pop(mix_comp_name, None)

    # Remove 'variable_components' (will be reformed by Recipe below).
    reduced_recipe_dict.pop("variable_components", None)

    mix_recipe = Recipe(**reduced_recipe_dict)

    return mix_recipe
