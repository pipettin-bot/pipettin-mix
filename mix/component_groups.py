# Copyright (C) 2023 Nicolás A. Méndez
# 
# This file is part of "mix".
# 
# "mix" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "mix" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "mix". If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import numpy as np
from itertools import product
from pprint import pprint
from copy import deepcopy
from collections import OrderedDict as odict

def all_equal(x):
    """Check if all elements in a list are equal using 'set'.

    Args:
        x (list): A flat list, or dict coherced to list internally.

    Returns:
        bool: True if all elements are equal.
    """
    if len(list(x)) <= 1:
        return True
    else:
        return len(list(set(list(x)))) == 1

def groups(**kwargs):
    """Generate dict from keyword args."""
    return odict((k, v) for k,v in kwargs.items())

def group(**kwargs):
    """Generate dict from keyword args.

    Note: relies on the "c" function defined below.
    
    Examples:
        component_groups = groups(
        # https://gitlab.com/darksideoftheshmoo/rtcc/-/issues/62#note_650212359
        Whi5_mCherry_tag1 = group( template="A139", pFw = 432, pRv = c(433, 282) ),
        Whi5_mCherry_tag2 = group( template=c("A709", "A710", "A602"), pFw = 1191, pRv = c(433, 282) ),
        
        # https://gitlab.com/darksideoftheshmoo/rtcc/-/issues/62#note_668117282 
        Far1_mNG_tag = group( template=c("A486", "A487", "A492"), pFw = 240, pRv=241 ),
        
        # https://gitlab.com/darksideoftheshmoo/rtcc/-/issues/79
        HTA_CFP_tag = group( template="A550", pFw = 667, pRv = 668 )
        )
    """
    od = odict((k, v) for k,v in kwargs.items())  # Preserve order
    for key in list(od):
        od[key] = c(od[key])
    return od

def c(*args):
    """Convert input to a tuple of string values.
    
    Limitation: input can either be several atomic values (not lists) or a single list of atomic values.
    
    Examples:
        x = 486
        pprint.pprint(c(x))
        pprint.pprint(c(c(x)))

        x = [486, "A487", "A492"]
        pprint.pprint(c(x))
        pprint.pprint(c(c(x)))
    """
    if len(args) > 1:
        result = tuple([str(arg) for arg in args])
    elif isinstance(args[0], (list, tuple)):
        result = tuple([str(arg) for arg in args[0]])
    else:
        result = tuple([str(args[0])])
        
    return result


def expand_grid(dictionary):
    """Python version of R's 'expand.grid'.
    Source: https://stackoverflow.com/a/32549843/11524079
    Example:
        dictionary = {'color': ['red', 'green', 'blue'], 
                'vehicle': ['car', 'van', 'truck'], 
                'cylinders': [6, 8]}
        expand_grid(dictionary)
    """
    return pd.DataFrame(data=[row for row in product(*dictionary.values())], 
                        columns=dictionary.keys())

def expand_groups(component_groups: dict,
                  group_label:str="group", 
                  label_col:str="label", 
                  replace_missing="0",
                  keep_constants=False):
    """Run expand_grid on each group and bind the rows.
    
    Used by 'get_variable_components'.

    Note that 'expand_groups' only generates 'variable_components' which effectively
    generate combinations (during expand_grid) Because of this a _constant component_ 
    (i.e. single-valued) will not be kept, and will not appear in the _variable components_ dict.
    Set `keep_constants=True` to disable this behaviour.

    Example:
        groups_df, value_columns, variable_columns, variable_components = expand_groups(COMPONENT_GROUPS)
        print(groups_df)
    """

    component_groups = deepcopy(component_groups)

    df_list = []
    for group_name in list(component_groups):
        g = {group_label: [group_name]}  # Needs to be in [list] form.
        g.update(component_groups[group_name])
        
        df = expand_grid(g)
        df = df.reset_index(drop=True)  # make sure indexes pair with number of rows
        
        df_list.append(df)

    # https://stackoverflow.com/a/51896661/11524079
    result_df = pd.concat(df_list, axis=0, ignore_index=True)
    result_df.replace(np.nan, replace_missing, inplace=True)
    result_df = result_df.reset_index(drop=True)
    
    # Get variable colums.
    value_columns = [n for n in result_df.columns if n not in [group_label, group]]
    variable_columns = [n for n in value_columns if not all_equal(result_df[n].values) or keep_constants]
    variable_components = odict( (var, get_comp_variants(component_groups, var)) for var in variable_columns )
    
    # Make unique label column.
    result_df[label_col] = ["_".join([str(i) for i in row if i != row[group_label]]) for index, row in result_df.iterrows()]
    
    return result_df, value_columns, variable_columns, variable_components

def get_comp_variants(component_groups, comp_name):
    """Get all unique variants of a component from the 'component_groups' dictionary.
    For example, get all 'templates' from the dictionary."""
    variants = []
    for component_group in component_groups.values():
        variants = variants + list(component_group.get(comp_name, []))
    return tuple(set(variants))

COMPONENT_GROUPS = groups(
  # https://gitlab.com/darksideoftheshmoo/rtcc/-/issues/62#note_650212359
  Whi5_mCherry_tag1 = group( template="A139", pFw = 432, pRv = c(433, 282) ),
  Whi5_mCherry_tag2 = group( template=c("A709", "A710", "A602"), pFw = 1191, pRv = c(433, 282) ),
  
  # https://gitlab.com/darksideoftheshmoo/rtcc/-/issues/62#note_668117282 
  Far1_mNG_tag = group( template=c("A486", "A487", "A492"), pFw = 240, pRv=241 ),
  
  # https://gitlab.com/darksideoftheshmoo/rtcc/-/issues/79
  HTA_CFP_tag = group( template="A550", pFw = 667, pRv = 668 )
)
