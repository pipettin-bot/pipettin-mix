# Copyright (C) 2023 Nicolás A. Méndez
# 
# This file is part of "mix".
# 
# "mix" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "mix" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "mix". If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import numpy as np
from scipy.cluster.hierarchy import linkage, fcluster, dendrogram
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt
from scipy.spatial.distance import squareform

from .component_groups import expand_groups

try:
    import gower
except ImportError:
    print("Cluster warning: gower package not available, 'use_gower' must be set to false.")

# method (str, optional): Method to compute the distance between two clusters: single, complete, average, weighted, centroid, median, ward. Defaults to 'average'.
# metric (str, optional): Distance metric to use: 'braycurtis', 'canberra', 'chebyshev', 'cityblock', 'correlation', 'cosine', 'dice', 'euclidean', 'hamming', 'jaccard', 'jensenshannon', 'kulczynski1', 'mahalanobis', 'matching', 'minkowski', 'rogerstanimoto', 'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath', 'sqeuclidean', 'yule'. Defaults to 'euclidean'.
# metrics = ['braycurtis', 'canberra', 'chebyshev', 'cityblock', 'correlation', 'cosine', 'dice', 'euclidean', 'hamming', 'jaccard', 'jensenshannon', 'kulczynski1', 'mahalanobis', 'matching', 'minkowski', 'rogerstanimoto', 'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath', 'sqeuclidean', 'yule']

# for metric in metrics:
#     print(metric)
#     try:
#         cluster = Cluster(data_frame=df, use_gower=False, 
#                           method="complete", metric=metric, #"minkowski", 
#                           id_cols=['label', 'group'])
#         cluster.plot_dendrogram()
#     except:
#         print(f"failed with metric {metric}")

def cluster_components(component_groups):
    """Setup component clusters with nice defaults.

    Args:
        component_groups (_type_): _description_

    Returns:
        _type_: _description_
    """
    
    cluster = Cluster(component_groups=component_groups, # data_frame=self.groups_df,
                      use_gower=False,
                      method="complete", metric="euclidean", 
                      optimal_ordering=True,
                      label_col = 'label',
                      group_col = 'group')
    
    return cluster


class Groups():
    """Uses 'expand_groups' on  'component_groups' and saves the results in atributes."""
    def __init__(self, component_groups, 
                 group_label="group", label="label", 
                 replace_missing="0", 
                 keep_constants=True):
        self.component_groups = component_groups
        self.group_label = group_label
        self.label = label
        self.replace_missing = replace_missing
        # Outputs
        self.value_columns = None
        self.variable_columns = None
        self.variable_components = None
        self.keep_constants=keep_constants
        
        # Save the result
        self.groups_df, self.value_columns, self.variable_columns, self.variable_components = expand_groups(
            component_groups=self.component_groups,
            group_label=self.group_label,
            label_col=self.label,
            replace_missing=self.replace_missing,
            keep_constants=self.keep_constants
        )

    # Save the expand_groups function "as a static method".
    def expand_groups(*args, **kwargs):
        return expand_groups(*args, **kwargs)


class Cluster():
    """Cluster a dataframe treating all variables as categorical.
    
    Example:
    
    # create a sample dataframe
    data = {'Name': ['Alice', 'Bob', 'Charlie', 'Dave', 'Eve', 'Frank'],
            'Gender': ['Female', 'Male', 'Male', 'Male', 'Female', 'Male'],
            'Nationality': ['USA', 'USA', 'Canada', 'Canada', 'USA', 'Canada'],
            'Occupation': ['Student', 'Engineer', 'Engineer', 'Doctor', 'Student', 'Doctor'],
            'Salary': [20000, 50000, 45000, 70000, 25000, 80000]}

    df = pd.DataFrame(data)

    cluster = Cluster(data_frame=df, id_cols=['Name'])

    cluster.plot_dendrogram()
    """
    def __init__(self, 
                 data_frame=None, component_groups=None,
                 replace_missing="0",
                 use_gower=False,
                 method='complete', metric='euclidean',
                 optimal_ordering=True,
                 label_col = 'label',
                 group_col = 'group',
                 id_cols = None,
                 draw_dendrogram=False):
        """_summary_

        Args:
            data_frame (_type_): A table with data and an ID column.
            replace_missing (str, optional): Argument passed to 'expand_groups' when making 'Groups'. Used to fill keys missing in other groups.
            label_col (str, optional): Names of the 'label' ID column in the dataframe. Defaults to 'label'.
            group_col (str, optional): Names of the 'group' ID column in the dataframe. Defaults to 'group'.
            id_cols (list, optional): Names of other ID columns in the dataframe. Defaults to None.
            method (str, optional): Method to compute the distance between two clusters: single, complete, average, weighted, centroid, median, ward. Defaults to 'complete'.
            metric (str, optional): Distance metric to use: 'braycurtis', 'canberra', 'chebyshev', 'cityblock', 'correlation', 'cosine', 'dice', 'euclidean', 'hamming', 'jaccard', 'jensenshannon', 'kulczynski1', 'mahalanobis', 'matching', 'minkowski', 'rogerstanimoto', 'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath', 'sqeuclidean', 'yule'. Defaults to 'euclidean'.
            use_gower (bool, optional): Use gower.gower for calculating distances (its a bit hacky). Defaults to False.
        """        
        
        #_summary_

        # Args:
        #     data_frame (pandas dataframe): 
        #     id_col (str, optional): 
            
            
        # """
        if component_groups:
            self.groups = Groups(component_groups=component_groups, 
                                 group_label=group_col, label=label_col, 
                                 replace_missing=replace_missing)

            data_frame = self.groups.groups_df
        elif not data_frame:
            raise Exception("Cluster: Either component_grups or data_frame must be provided.")
        
        if id_cols is None:
            id_cols = []
        self.id_cols = [label_col, group_col] + id_cols

        self.data_frame = data_frame.copy()
        self.gower_distance_matrix = None
        
        # Get categorical columns
        self.df_cat_vars = self.data_frame.loc[:, np.invert(self.data_frame.columns.isin(self.id_cols))]
        
        # Select a distance
        if use_gower:
            self.Z = self.gower_distance(method=method, metric=metric, optimal_ordering=optimal_ordering)
        else:
            self.Z = self.other_distance(method=method, metric=metric, optimal_ordering=optimal_ordering)

        # Cluster from predefined K number:
        # # set the number of clusters to form
        # num_clusters = 7
        # 
        # # assign cluster labels
        # labels = fcluster(Z, num_clusters, criterion='maxclust')
        # 
        # # add cluster labels to the original dataframe
        # df[f'Cluster'] = labels
        # 
        # for n in range(len(list(data))+1):
        #     # assign cluster labels
        #     labels = fcluster(Z, n+1, criterion='maxclust')

        #     # add cluster labels to the original dataframe
        #     df[f'Cluster{n+1}'] = labels

        # Prepare a list for the cluster labels
        cluster_names = []

        # Cluster by the heights of the branching points:
        
        # Cut the base tree (i.e. at the lowest level, terminal leafs will be grouped here).
        labels = self.group_by_height(height=0.0)
        cluster_name = f'Cluster{0}'
        cluster_names.append(cluster_name)
        self.data_frame[cluster_name] = labels

        # Get the (unique) heights of each cluster
        self.cluster_heights = set(self.Z[:,2])
        # Cut the dendrogram at each height, and
        # save the cluster groups to the dataframe.
        for i, height in enumerate(self.cluster_heights):
            # print(f"Cluster {i+1} height: {height}")
            labels = self.group_by_height(height=height)
            cluster_name = f'Cluster{i+1}'
            cluster_names.append(cluster_name)
            self.data_frame[cluster_name] = labels

        # Reverse cluster_names to get the highest / coarse groups first.
        cluster_names.reverse() # Reverses list in place.
        # Save the cluster names
        self.cluster_names = cluster_names
        
        if draw_dendrogram:
            self.plot_dendrogram()

    def gower_distance(self, **kwargs):
        # Google code:
        # Calculate the gower distance on the data.
        self.gower_distance_matrix = gower.gower_matrix(self.df_cat_vars).astype(float)
        # Convert the redundant n*n square matrix form into a condensed nC2 array,
        # containing the values of the upper triangle.
        # https://stackoverflow.com/a/18954990/11524079
        Z_gower = squareform(self.gower_distance_matrix)
        # This flat array can be passed to linkage to generate an appropriate input for fcluster
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
        
        # The input y may be either a 1-D condensed distance matrix or a 2-D array of observation vectors.
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
        Z_gower = linkage(y=Z_gower, **kwargs)
        # AgglomerativeClustering
        
        return Z_gower
    
    def other_distance(self, **kwargs):
        # GPT code:
        # convert categorical columns to numerical using label encoding
        # le = LabelEncoder()
        # for col in self.df_cat_vars.columns:
        #     self.df_cat_vars[col] = le.fit_transform(self.df_cat_vars[col])
        
        # Convert categorical columns to one-hot
        self.df_cat_vars = pd.get_dummies(self.df_cat_vars)
        
        # perform hierarchical clustering
        Z_ward = linkage(self.df_cat_vars, **kwargs)
        
        return Z_ward
    
    def get_result(self):
        return self.data_frame.copy()
    
    def group_by_height(self, height):
        return fcluster(self.Z, height, criterion='distance')
    
    def plot_dendrogram(self, label_col=None, label_rotation=90, **kwargs):
        if not label_col:
            label_col = self.id_cols[0]
        # Generate dendrogram: https://www.python-graph-gallery.com/401-customised-dendrogram
        dendrogram(self.Z, leaf_rotation=label_rotation,
                   labels=self.data_frame[label_col].tolist(),
                   **kwargs)

        # set x-axis label and y-axis label
        plt.xlabel('Sample Index')
        plt.ylabel('Distance')

        # show the plot
        plt.show()

