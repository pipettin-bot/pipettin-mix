# Copyright (C) 2023 Nicolás A. Méndez
#
# This file is part of "mix".
#
# "mix" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "mix" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "mix". If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=locally-disabled, line-too-long, logging-fstring-interpolation, logging-not-lazy, fixme, multiple-imports, wrong-import-position, bad-indentation

from dataclasses import dataclass
import numpy as np
from copy import deepcopy
import json
from collections import OrderedDict as odict

from .component_groups import *
from .cluster import *
from .cuentitas import *

# @staticmethod
def classify_components(component_names: list,
                        comp_groups_df: pandas.DataFrame):
  # Remove ID columns (e.g. group, label, an cluster columns) which are not data.
  # id_cols = self.comp_groups.id_cols + self.comp_groups.cluster_names
  # comps_df = self.remove_cols(self.comp_groups_df, id_cols)

  # Get columns for the variable components
  comps_df = comp_groups_df.loc[:, comp_groups_df.columns.isin(component_names)]

  # Get component names that are contstant and variable.
  # From 'get_variable_components'.
  fixed_comps = [col for col in comps_df if all_same(comps_df[col].tolist())]
  var_comps = [col for col in comps_df if not all_same(comps_df[col].tolist())]

  return fixed_comps, var_comps

@dataclass  # https://stackoverflow.com/a/71189560/11524079
class MixPlan:
  # IN USE:
  master_recipe: Recipe
  preparations: list = None
  fix_comps_ref: dict = None # Indicates which components are newly fixed.
  plot_dendrogram: bool = False
  print_recipes: bool = False
  parent_mixplan: object = None
  fading_rate: float = 0.2 # A recipe's extra_fraction will be diminished by this fraction at each mix level.
  verbose: bool = False
  # IN USE BUT OVERWRITTEN IMMEDIATELY:
  top_cluster_label: str = None
  # NOT USED YET:
  print_output: bool = False
  # cut_level: int = 1
  # cleanup: bool = None
  # level_prefix: str = "level_"
  # branch_prefix: str = "branch_"
  # master_mix_name: str = "level_mastermix"

  def __post_init__(self):
    """Generates compositions of intermediate mixes and a pipetting plan.
    Migration of 'cuentitas5' from the R implementation.
    """

    # Create the main "master" mix.
    self.master_mix = Mix(self.master_recipe,
                          print_recipe=self.print_recipes,
                          fix_comps_ref=self.fix_comps_ref)

    # Cluster component groups.
    self.comp_groups = Cluster(component_groups=self.master_recipe.component_groups,
                               draw_dendrogram=self.plot_dendrogram)
    # Extract the groups dataframe.
    self.comp_groups_df = self.comp_groups.get_result()

    # Group the components dataframe by the first differential cluster label.
    self.top_cluster_label = self.get_top_cluster()

    # Group the clustered components dataframe by the differentiating cluster.
    # There should always be the "terminal cluster" which differentiates all leaves
    # of the dendrogram.
    comps_df_grouped = self.comp_groups_df.groupby(self.top_cluster_label)

    # Generate child mixes and corresponding MixPlan objects recursively.
    self.children = []
    for group_name, derivative_comps_df in comps_df_grouped:
      child = self.add_child_plan(group_name, derivative_comps_df)
      # Append the "child" and metadata to the list.
      self.children.append(child)

  def add_child_plan(self, group_name: str, derivative_comps_df: pandas.DataFrame) -> dict:
    """Create a child item for a group of components and add it to the list."""
    # Check if this is a terminal group.
    has_children = len(derivative_comps_df.index) > 1
    tube_label=None

    # Make new "Recipe" and "Mix" objects.
    child_recipe, child_mix = self.make_child_mix(derivative_comps_df,
                                                  self.comp_groups_df)

    # Check that this is not a terminal group. Run MixPlan if not.
    if not has_children:
      # A single reaction is left, no planning left to do.
      child_mix_plan = None
      # Tube label
      tube_label = derivative_comps_df["label"].iloc[0]
      # pprint(f"tube_label: {tube_label}")
      # pprint(f"derivative_comps_df:")
      # pprint(derivative_comps_df)
    else:
      # Build child mix plan.
      # Note that calling MixPlan has its recursive behaviour here.
      child_mix_plan = MixPlan(
        master_recipe = child_recipe,
        fix_comps_ref = child_mix.mix_info["general_info"]["fix_comps_ref"],
        parent_mixplan = self,  # Let the child store a link to its parent.
        print_output = self.print_output,
        plot_dendrogram = self.plot_dendrogram
        # cut_level = self.cut_level,
        # cleanup = self.cleanup,
        # level_prefix = self.level_prefix,
        # branch_prefix = self.branch_prefix,
        # master_mix_name = self.master_mix_name
      )

    # Save the child MixPlan along with useful metadata.
    child = {"cluster": self.top_cluster_label,
             "group_name": group_name,
             "parent_mix_plan": self.parent_mixplan,
             "child_recipe": child_recipe,
             "child_mix": child_mix,
             "child_mix_plan": child_mix_plan}
    # Done!
    return child

  def make_child_mix(self, derivative_comps_df, comp_groups_df, compname_sep="_"):
    # Check if this is a terminal group.
    has_children = len(derivative_comps_df.index) > 1

    # Get the component names from the "parent" Recipe.
    original_component_names = self.master_recipe.variable_component_names  # Should be more complete

    # Get the old component classification.
    _, var_comps_old = classify_components(original_component_names, comp_groups_df)
    # print(fixed_comps_old, var_comps_old)

    # Get the new component classification.
    _, var_comps_new = classify_components(original_component_names, derivative_comps_df)
    # print(fixed_comps_new, var_comps_new)

    # Get the variable components that changed.
    new_fix_comps = setdiff(var_comps_old, var_comps_new)
    # print(new_fix_comps)

    # Make a dictionary of component names and the corresponding fixed variants.
    new_fix_comps_dict = odict( (k, derivative_comps_df[k].tolist()[0]) for k in new_fix_comps )
    # Create new names for the new variable components.
    # new_fix_comps_names = {k: f"{k}{compname_sep}{derivative_comps_df[k].tolist()[0]}" for k in new_fix_comps}

    # Serialize the reduced "master mix" recipe (produced by Mix above).
    reference_recipe_dict = self.master_mix.mix_recipe.to_dict()

    # Update reaction counts.
    reference_recipe_dict["n_reactions"] = len(derivative_comps_df.index)  # Use the row count
    # Fade extra_fraction at each new child mix.
    new_fraction = reference_recipe_dict["extra_fraction"] * (1.0 - self.fading_rate)
    # Force the fraction to zero if this is a terminal mix (and has no children).
    reference_recipe_dict["extra_fraction"] = new_fraction * has_children  # Zero if false
    # Update n_extra.
    n_extra = reference_recipe_dict["n_reactions"] * reference_recipe_dict["extra_fraction"]
    reference_recipe_dict["n_extra"] = n_extra

    # Generate new component groups
    reference_recipe_dict["component_groups"] = groups_to_comps(derivative_comps_df, var_comps_new)

    # Remove variable components (they will be reformed by 'Recipe' below).
    reference_recipe_dict.pop("variable_components")

    child_recipe = Recipe(**reference_recipe_dict)
    child_mix = Mix(child_recipe, print_recipe=self.print_recipes, fix_comps_ref=new_fix_comps_dict)

    return deepcopy((child_recipe, child_mix))  # copy just in case

  @staticmethod
  def remove_cols(df, col_names):
    """Utility method that makes me want to move back to R."""
    return df.loc[:, np.invert(df.columns.isin(col_names))]

  @staticmethod
  def get_cols(df, col_names):
    """Utility method that makes me want to move back to R."""
    return df.loc[:, df.columns.isin(col_names)]

  def get_top_cluster(self):
    """Group the components dataframe by the first differential cluster label."""
    top_cluster_label = None
    for cluster_name in self.comp_groups.cluster_names:
      group_labels = self.comp_groups_df[cluster_name].tolist()
      if len(group_labels) <= 1:
        # If only one row is left, then there is no differentiating cluster.
        break
      if not all_equal(group_labels):
        top_cluster_label = cluster_name
        break

    if not top_cluster_label:
      if self.verbose:
        print("Message: Terminal group. No remaining clusters, all solutions are identical.")
    elif top_cluster_label == self.comp_groups.cluster_names[-1]:
      if self.verbose:
        print(f"Message: Terminal group. No remaining clusters after: {top_cluster_label}")
    else:
      if self.verbose:
        print(f"Message: clustering by: {top_cluster_label}")

    return top_cluster_label

  def make_preparation(self, base_mix_name="main", parent_mix_name=None, preparations=None, level=0, parent_label=""):
    """Pseudo-recursive method that triggers calls on the same method of 'child' MixPlan objects."""

    mix_comp_vols = self.master_mix.mix_info["mix_component_volumes"]
    fix_comps_ref = self.master_mix.mix_info["general_info"]["fix_comps_ref"]
    if parent_mix_name:
      # If a parent was provided, make a new label for the child mix, indicating its parent.
      label = "-".join([parent_label] + [ f"{k}{v}" for k, v in fix_comps_ref.items() ])
    else:
      # If no parent was provided, use the default label for mixes.
      label = base_mix_name

    # Write the preparation.
    preparation = {"name": base_mix_name,
                   "parent": parent_mix_name,
                   "recipe": mix_comp_vols,
                   "fixed_component_reference": fix_comps_ref,
                   "label": label,
                   "derivatives": []}
    preparation["volume"] = sum(preparation["recipe"].values())

    # Initiate list of preparations.
    if preparations is None:
      self.preparations = preparations = []

    # Append the reduced preparation.
    p = {'name': preparation['name'],
         'recipe': preparation['recipe'],
         'level': level}
    preparations.append(p)

    # Make derivative preparations recursively.
    for i, child in enumerate(self.children):
      # Get the MixPlan object from the 'child'.
      child_plan: MixPlan = child["child_mix_plan"]
      if child_plan:
        # Make a new name from lowercase letters.
        mix_name = f"{base_mix_name}.{chr(97 + i)}"
        # Call this method (recursively) while there are children.
        child_prep = child_plan.make_preparation(base_mix_name=mix_name,
                                                 parent_mix_name=base_mix_name,
                                                 preparations=preparations, level=level+1,
                                                 parent_label=label)
      else:
        # Make a new name for terminal samples.
        mix_name = label
        # Make a prep from the terminal Mix object (i.e. with no children).
        child_mix: Mix = child["child_mix"]
        child_prep = child_mix.make_preparation(base_mix_name=mix_name,
                                                parent_mix_name=base_mix_name,
                                                preparations=preparations, level=level+1,
                                                parent_label=label)

      # Update the name of the parent mix to have its name.
      child_prep["recipe"][base_mix_name] = child_prep["recipe"].pop("mix", None)
      child_prep["recipe"].move_to_end(base_mix_name, last=False)

      # Save the prep.
      preparation["derivatives"].append(child_prep)

    return preparation

  @staticmethod
  def write_preparation(preparation: dict, file="preparation.json"):
      with open(file, 'w', encoding='utf-8') as f:
        json.dump(preparation, f, ensure_ascii=False, indent=2)


# def mix_plan(components: Recipe, extra_fraction = 0.2):

#   components = deepcopy(components)

#   # Cluster component groups
#   grupos = expand_groups(components.component_groups)
#   grupos_cl = cluster_components(components.component_groups)

#   # Get amount of reactions
#   n_reactions = grupos_cl.shape[0]
#   n_extra = components.n_reactions * components.extra_fraction

#   # Prepare main result dict
#   result = {"h.clust": grupos_cl, "grupos": grupos}

#   # Generate "variable_components" from "component_groups"
#   variable_components, variable_component_names, grupos = get_variable_components(components.component_groups, grupos)

#   return variable_components
