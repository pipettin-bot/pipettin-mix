# Copyright (C) 2023 Nicolás A. Méndez
# 
# This file is part of "mix".
# 
# "mix" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "mix" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "mix". If not, see <https://www.gnu.org/licenses/>.

from pprint import pprint
from .component_groups import groups, group, c
from .cluster import Cluster, Groups
from .cuentitas import *
from .mix_plan import *

component_groups = groups(
    reaccion1 = group( template="A550", pFw = c(667, 999), pRv = 668 ),
    reaccion2 = group( template="A550", pFw = 667, pRv = c(668, 770), dmso=10 )
)

master_recipe = Recipe(
  component_groups = component_groups,
  volume = 50,
  n_reactions = 10,
  n_extra = 2,
  stock_solutions_x = comps(
    buffer = 10,      # 10 x
    pol = 100,        # 100x (0.2 uL / 20 uL = 0.01)
    dNTPs = 10,       # 10 mM stock
    pFw = 10,         # uM
    pRv = 10,         # uM
    dmso = 100,
  ),
  stock_solutions_vol = comps(
    template = 1      # uL
  ),
  target_solution = comps(
    dNTPs = 0.1,      # 0.1 mM final (10 mM is 100x). Recomiendan 50-200 uM, normalmente uso 0.2 final / 50 x.
    pFw = 0.2,        # uM (50 x)
    pRv = 0.2         # uM (50 x)
  )
)

if __name__ == "__main__":
  mix_plan = MixPlan(master_recipe)
  prep = mix_plan.make_preparation()
  print("\n\n#### Preparation sequence test result ####\n")
  pprint(prep)
  print("\n\n#### Done! ####\n")
